# -*- coding: utf-8 -*-

"""Setuptools setup file, used to install or test ``mrmath``.

.. moduleauthor:: Michael Rippstein <info@anatas.ch>
"""

from setuptools import setup

setup()

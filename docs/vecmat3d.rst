3D-vektor- und matrix- klasen und funktionen
============================================

Module :mod:`mrmath.vecmat3d`
-----------------------------

.. automodule:: mrmath.vecmat3d

Class :class:`Vec3D`
~~~~~~~~~~~~~~~~~~~~

.. autoclass:: mrmath.vecmat3d.Vec3D
   :members:
   :undoc-members:
   :no-special-members:
   :no-private-members:

Class :class:`Mat3D`
~~~~~~~~~~~~~~~~~~~~

.. autoclass:: mrmath.vecmat3d::Mat3D
   :members:
   :undoc-members:
   :no-private-members:

Operationen
~~~~~~~~~~~

.. autofunction:: mrmath.vecmat3d::cross
.. autofunction:: mrmath.vecmat3d::dot
.. autofunction:: mrmath.vecmat3d::transp
.. autofunction:: mrmath.vecmat3d::einmat
.. autofunction:: mrmath.vecmat3d::rotmatx
.. autofunction:: mrmath.vecmat3d::rotmaty
.. autofunction:: mrmath.vecmat3d::rotmatz
.. autofunction:: mrmath.vecmat3d::polar2kart

.. mrmath documentation master file, created by
   sphinx-quickstart on Mon Nov  5 20:59:40 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mrmath's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:

   mrmath

   vecmat3d

   todo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

:mod:`mrmath` mathematische-funktionen
======================================

.. automodule:: mrmath

Konstanten
----------
Die Konstanten werden von dem Modul :mod:`mrmath` bereitgestellt.

.. autodata:: mrmath._const.RAD
.. autodata:: mrmath._const.DEG
.. autodata:: mrmath._const.ARCS

Hyperbelfunktionen
------------------

.. autofunction:: mrmath.coth

Zahlenteoretische und "darstellende" Funktionen
-----------------------------------------------

.. autofunction:: mrmath.frac
.. autofunction:: mrmath.modulo
.. autofunction:: mrmath.iseven
.. autofunction:: mrmath.isodd

Winkelumrechnungen
------------------

.. autofunction:: mrmath.ddd
.. autofunction:: mrmath.dms

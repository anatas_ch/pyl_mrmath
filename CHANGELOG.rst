=========
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`__,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`__.


[Unreleased]
============

Added
-----

Changed
-------

Fixed
-----


[0.2.0] - 2022-02-21
====================

Changed
-------
- numpy required 1.22
- changed testframework to pytest
- add more test to mrmath module


[0.1.0] - 2022-01-18
====================

Added
-----
- first public release.
